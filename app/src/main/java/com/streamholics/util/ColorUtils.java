package com.streamholics.util;

import android.graphics.Color;

public class ColorUtils {
    private static final float LIGHT_BRIGHTNESS_FACTOR = 1.3f;
    private static final float DARK_BRIGHTNESS_FACTOR = 0.5f;

    public static int parseColor(String color) {
        String parsedColor = color.length() > 7 ? color.substring(0, color.length() - 2) : color;
        return Color.parseColor(parsedColor);
    }

    public static String normalizeColor(String color) {
        if (color.length() > 7)
            return color.substring(0, color.length() - 2);

        return color;
    }

    public static int brightenColor(int color) {
        return manipulateColor(color, LIGHT_BRIGHTNESS_FACTOR);
    }

    public static int darkenColor(int color) {
        return manipulateColor(color, DARK_BRIGHTNESS_FACTOR);
    }

    private static int manipulateColor(int color, float factor) {
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * factor);
        int g = Math.round(Color.green(color) * factor);
        int b = Math.round(Color.blue(color) * factor);
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255));
    }
}