package com.streamholics.util;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.streamholics.presentation.activity.R;

public class ToastUtils {
    private static Toast toast;

    public static void makeToast(Activity activity, int stringId) {
        if (toast != null) {
            toast.cancel();
            toast = null;
        }

        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_view,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));
        TextView text = layout.findViewById(R.id.text);
        text.setText(stringId);

        toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 350);
//        toast = Toast.makeText(context, stringId, Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}