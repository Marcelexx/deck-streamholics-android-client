package com.streamholics.util;

import android.app.Activity;
import android.content.Intent;

public class QRCodeUtils {
    private static final String QR_READER_PACKAGE = "com.google.zxing.client.android.SCAN";
    private static final String QR_READER_SCAN_MODE = "SCAN_MODE";
    private static final String QR_READER_QR_CODE_MODE = "QR_CODE_MODE";

    public static void startQRCodeScanner(Activity activity) {
        Intent intent = new Intent(QR_READER_PACKAGE);
        intent.putExtra(QR_READER_SCAN_MODE, QR_READER_QR_CODE_MODE);

        activity.startActivityForResult(intent, 0);
    }
}