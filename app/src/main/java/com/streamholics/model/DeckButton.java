package com.streamholics.model;

public class DeckButton {
    private int buttonId;
    private String buttonName;
    private String buttonColor;
    private String buttonTextColor;
    private String buttonVoicemeeterChannel;
    private String buttonTextPosition;
    private String buttonBorderColor;
    private String buttonCommandName;
    private String buttonCommandCode;
    private String buttonImagePath;
    private boolean buttonIsSelectable;
    private boolean buttonIsVoicemeeter;
    private double buttonVoicemeeterValue;

    public int getButtonId() {
        return buttonId;
    }

    public void setButtonId(int buttonId) {
        this.buttonId = buttonId;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonColor() {
        return buttonColor;
    }

    public void setButtonColor(String buttonColor) {
        this.buttonColor = buttonColor;
    }

    public String getButtonTextColor() {
        return buttonTextColor;
    }

    public void setButtonTextColor(String buttonTextColor) {
        this.buttonTextColor = buttonTextColor;
    }

    public String getButtonTextPosition() {
        return buttonTextPosition;
    }

    public void setButtonTextPosition(String buttonTextPosition) {
        this.buttonTextPosition = buttonTextPosition;
    }

    public String getButtonBorderColor() {
        return buttonBorderColor;
    }

    public void setButtonBorderColor(String buttonBorderColor) {
        this.buttonBorderColor = buttonBorderColor;
    }

    public String getButtonCommandName() {
        return buttonCommandName;
    }

    public void setButtonCommandName(String buttonCommandName) {
        this.buttonCommandName = buttonCommandName;
    }

    public String getButtonCommandCode() {
        return buttonCommandCode;
    }

    public void setButtonCommandCode(String buttonCommandCode) {
        this.buttonCommandCode = buttonCommandCode;
    }

    public String getButtonImagePath() {
        return buttonImagePath;
    }

    public void setButtonImagePath(String buttonImagePath) {
        this.buttonImagePath = buttonImagePath;
    }

    public boolean getButtonIsSelectable() {
        return buttonIsSelectable;
    }

    public void setButtonIsSelectable(boolean buttonIsSelectable) {
        this.buttonIsSelectable = buttonIsSelectable;
    }

    public boolean getButtonIsVoicemeeter() {
        return buttonIsVoicemeeter;
    }

    public void setButtonIsVoicemeeter(boolean buttonIsVoicemeeter) {
        this.buttonIsVoicemeeter = buttonIsVoicemeeter;
    }

    public double getButtonVoicemeeterValue() {
        return buttonVoicemeeterValue;
    }

    public void setButtonVoicemeeterValue(double buttonVoicemeeterValue) {
        this.buttonVoicemeeterValue = buttonVoicemeeterValue;
    }

    public String getButtonVoicemeeterChannel() {
        return buttonVoicemeeterChannel;
    }

    public void setButtonVoicemeeterChannel(String buttonVoicemeeterChannel) {
        this.buttonVoicemeeterChannel = buttonVoicemeeterChannel;
    }
}