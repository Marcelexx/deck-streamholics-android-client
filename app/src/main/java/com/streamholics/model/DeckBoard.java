package com.streamholics.model;

import java.io.Serializable;
import java.util.List;

public class DeckBoard implements Serializable {
    private int id;
    private String boardName;
    private List<DeckButton> deckButtonList;
    private String boardColor;
    private int position;
    private String layout;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public final String getBoardName() {
        return boardName;
    }

    public final void setBoardName(final String boardName) {
        this.boardName = boardName;
    }

    public List<DeckButton> getDeckButtonList() {
        return deckButtonList;
    }

    public void setDeckButtonList(List<DeckButton> deckButtonList) {
        this.deckButtonList = deckButtonList;
    }

    public String getBoardColor() {
        return boardColor;
    }

    public void setBoardColor(String boardColor) {
        this.boardColor = boardColor;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }
}