package com.streamholics.common;

public class Constants {
    public static String ADMOB_INTERSTITIAL_TEST_DEVICE = "1CB8599BBB52022A256E9F9F1891CF25";

    public static String BUTTON_TAG_PREFIX = "button";

    public static String JSON_BOARD_BEGINNING_WORD = "beginOfJson";

    public static String JSON_BOARD_ENDING_WORD = "endOfJson";
}