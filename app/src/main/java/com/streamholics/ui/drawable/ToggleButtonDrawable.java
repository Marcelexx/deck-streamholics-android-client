package com.streamholics.ui.drawable;

import android.graphics.drawable.Drawable;

import com.streamholics.util.ColorUtils;

import top.defaults.drawabletoolbox.DrawableBuilder;

public class ToggleButtonDrawable {
    private static final int CORNER_RADIUS = 20;
    private static final int GRADIENT_ANGLE = 45;

    public static Drawable createDrawable(int buttonColor, int borderColor, boolean isRipple) {
        DrawableBuilder drawableBuilder = new DrawableBuilder()
                .rectangle()
                .strokeWidth(0)
                .strokeColor(borderColor)
                .cornerRadius(CORNER_RADIUS)
                .gradient()
                .gradientColors(buttonColor, ColorUtils.brightenColor(buttonColor), buttonColor)
                .linearGradient()
                .angle(GRADIENT_ANGLE);

        if (isRipple) {
            drawableBuilder.ripple();
            drawableBuilder.rippleColor(0xffffffff);
        }

        return drawableBuilder.build();
    }
}