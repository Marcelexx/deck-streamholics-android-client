package com.streamholics.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class LoadingOverlay {
    private static final float BACKGROUND_ALPHA = 0.4f;
    private static final int ANIMATION_DURATION = 100;

    public static void startLoading(Activity activity, View loadingMainView) {
        activity.runOnUiThread(() -> animateView(loadingMainView, View.VISIBLE, BACKGROUND_ALPHA));
    }

    public static void stopLoading(Activity activity, View loadingMainView) {
        activity.runOnUiThread(() -> animateView(loadingMainView, View.GONE, 0));
    }

    private static void animateView(final View viewToShow, final int toVisibility, float toAlpha) {
        View viewToAlpha = null;

        for (int index = 0; index < ((ViewGroup) viewToShow).getChildCount(); index++) {
            View nextChild = ((ViewGroup) viewToShow).getChildAt(index);

            if (nextChild instanceof FrameLayout) {
                viewToAlpha = nextChild;
                break;
            }
        }

        if (viewToAlpha != null) {
            boolean show = toVisibility == View.VISIBLE;
            if (show)
                viewToAlpha.setAlpha(0);

            viewToShow.setVisibility(toVisibility);
            viewToAlpha.animate()
                    .setDuration(ANIMATION_DURATION)
                    .alpha(show ? toAlpha : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            viewToShow.setVisibility(toVisibility);
                        }
                    });
        }
    }
}