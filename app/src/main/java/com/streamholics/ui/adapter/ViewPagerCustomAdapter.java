package com.streamholics.ui.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.streamholics.model.DeckBoard;
import com.streamholics.presentation.fragment.LayoutFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ViewPagerCustomAdapter extends FragmentPagerAdapter {
    private List<DeckBoard> mBoardList;

    public void setBoardList(String boardsJson) {
        Gson gson = new Gson();

        Type listType = new TypeToken<ArrayList<DeckBoard>>() {
        }.getType();

        mBoardList = gson.fromJson(boardsJson, listType);
    }

    public ViewPagerCustomAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @NonNull
    @Override
    public Fragment getItem(int pos) {
        return new LayoutFragment(mBoardList.get(pos));
    }

    @Override
    public int getCount() {
        return mBoardList.size();
    }
}