package com.streamholics.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.appcompat.widget.AppCompatToggleButton;

import com.streamholics.model.DeckButton;
import com.streamholics.ui.drawable.ToggleButtonDrawable;
import com.streamholics.util.ColorUtils;

public class DeckToggleButtonCustom extends AppCompatToggleButton implements Parcelable {
    public boolean isVoicemeeter;
    public String voicemeeterChannel;
    public double voicemeeterValue;

    private String buttonCommand;
    private String textPosition;
    private final AnimatorSet animatorSet = new AnimatorSet();

    public DeckToggleButtonCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        setText(null);
        setTextOn(null);
        setTextOff(null);
    }

    public String getButtonCommand() {
        return buttonCommand;
    }

    public void setButtonCommand(String buttonCommand) {
        this.buttonCommand = buttonCommand;
    }

    public void setButtonInfo(DeckButton deckButton) {
        String buttonColor = deckButton.getButtonColor();
        int parsedButtonColor = ColorUtils.parseColor(buttonColor);
        int parsedBorderColor = ColorUtils.darkenColor(parsedButtonColor);

        textPosition = deckButton.getButtonTextPosition();
        isVoicemeeter = deckButton.getButtonIsVoicemeeter();
        voicemeeterChannel = deckButton.getButtonVoicemeeterChannel();
        voicemeeterValue = deckButton.getButtonVoicemeeterValue();
        setTextColor(Color.parseColor(ColorUtils.normalizeColor(deckButton.getButtonTextColor())));
        setText(deckButton.getButtonName());
        setTextOff(deckButton.getButtonName());
        setBackground(ToggleButtonDrawable.createDrawable(parsedButtonColor, parsedBorderColor,
                !deckButton.getButtonIsSelectable()));
        setButtonCommand(deckButton.getButtonCommandCode());

        setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (deckButton.getButtonIsSelectable()) {
                int buttonIntColor = !isChecked ? parsedButtonColor :
                        ColorUtils.brightenColor(parsedButtonColor);

                setBackground(ToggleButtonDrawable.createDrawable(buttonIntColor, parsedBorderColor,
                        !deckButton.getButtonIsSelectable()));

                if (isChecked)
                    startAnimation();
                else
                    stopAnimation();
            }
        });
    }

    private void startAnimation() {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(this, "alpha", 1.0f, 0.8f);
        fadeOut.setDuration(300);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(this, "alpha", 0.8f, 1.0f);
        fadeIn.setDuration(300);

        animatorSet.play(fadeIn).after(fadeOut);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animatorSet.start();
            }
        });

        animatorSet.start();
    }

    private void stopAnimation() {
        animatorSet.removeAllListeners();
        animatorSet.cancel();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (textPosition != null) {
            switch (textPosition) {
                case "TOP_CENTER":
                    setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
                    break;
                case "BOTTOM_CENTER":
                    setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
                    break;
                default:
                    setGravity(Gravity.CENTER);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}