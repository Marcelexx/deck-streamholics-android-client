package com.streamholics.control;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesControl {
    private final String SHARED_PREFERENCE = "sharedPreference";
    private final String LAST_IP_PREFERENCE = "lastIpPreference";
    private static SharedPreferencesControl instance;

    private SharedPreferencesControl() {
    }

    public static SharedPreferencesControl getInstance() {
        if (instance == null)
            instance = new SharedPreferencesControl();

        return instance;
    }

    public void setLastIp(Context context, String ip) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE).edit();
        editor.putString(LAST_IP_PREFERENCE, ip);
        editor.apply();
    }

    public String getLastIp(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);
        return sharedPreferences.getString(LAST_IP_PREFERENCE, "");
    }
}