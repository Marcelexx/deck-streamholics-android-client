package com.streamholics.control;

import com.streamholics.event.CloseBoardActivityEvent;
import com.streamholics.event.ConnectionErrorEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class ConnectionControl {
    private final String ENCODING = "UTF-8";
    private final int SOCKET_PORT = 5000;
    private final int SOCKET_TIMEOUT = 1000;

    private static ConnectionControl instance;

    private Socket socket;

    private ConnectionControl() {
    }

    public static ConnectionControl getInstance() {
        if (instance == null)
            instance = new ConnectionControl();

        return instance;
    }

    public void startSocket(String ipAddress, IConnectionControl connectionControl) {
        new Thread(() -> {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ipAddress, SOCKET_PORT), SOCKET_TIMEOUT);

                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), ENCODING));
                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                String line = in.readLine();

                while (line != null && line.length() > 0) {
                    connectionControl.readJsonLine(line);
                    out.flush();
                    line = in.readLine();
                }

                in.close();
                out.close();
                stopSocket();
            } catch (ConnectException | SocketTimeoutException stex) {
                EventBus.getDefault().post(new ConnectionErrorEvent());
            } catch (Exception ex) {
                stopSocket();
                ex.printStackTrace();
            }
        }).start();
    }

    public void stopSocket() {
        try {
            if (socket.isConnected()) {
                socket.close();
                EventBus.getDefault().post(new CloseBoardActivityEvent());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        new Thread(() -> {
            try {
                if (socket.isConnected()) {
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                    out.println(message);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }).start();
    }
}