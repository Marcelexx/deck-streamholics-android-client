package com.streamholics.control;

public interface IConnectionControl {
    void readJsonLine(String line);
}