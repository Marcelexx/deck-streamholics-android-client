package com.streamholics.presentation.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.streamholics.control.ConnectionControl;
import com.streamholics.model.DeckBoard;
import com.streamholics.model.DeckButton;
import com.streamholics.presentation.activity.R;
import com.streamholics.presentation.dialog.VoicemeeterVolumeDialog;
import com.streamholics.ui.DeckToggleButtonCustom;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.streamholics.common.Constants.BUTTON_TAG_PREFIX;

public class LayoutFragment extends Fragment {
    private Context mContext;

    private GridLayout mButtonsGrid;
    private ConstraintLayout mMainLayout;
    private DeckBoard deckBoard;

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = context;
        super.onAttach(context);
    }

    public LayoutFragment(DeckBoard deckBoard) {
        this.deckBoard = deckBoard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutId = R.layout.fragment_3x3_layout;

        Activity activity = getActivity();

        if (activity != null) {
            switch (deckBoard.getLayout()) {
                case "3x3":
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    break;
                case "3x4":
                    layoutId = R.layout.fragment_3x4_layout;
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    break;
                case "4x3":
                    layoutId = R.layout.fragment_4x3_layout;
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    break;
                case "4x4":
                    layoutId = R.layout.fragment_4x4_layout;
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    break;
                case "4x6":
                    layoutId = R.layout.fragment_4x6_layout;
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    break;
                case "6x4":
                    layoutId = R.layout.fragment_6x4_layout;
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    break;
            }
        }

        View view = inflater.inflate(layoutId, container, false);

        loadElements(view);
        initData();

        return view;
    }

    private void loadElements(View view) {
        mMainLayout = view.findViewById(R.id.cl_main_layout);
        mButtonsGrid = view.findViewById(R.id.gl_buttons_grid);
    }

    private void initData() {
        if (deckBoard.getBoardColor() != null)
            mMainLayout.setBackgroundColor(Color.parseColor(normalizeColor(deckBoard.getBoardColor())));

        loadButtons(deckBoard.getDeckButtonList());
    }

    private void loadButtons(List<DeckButton> deckButtonList) {
        for (DeckButton deckButton : deckButtonList) {
            for (int i = 0; i < mButtonsGrid.getChildCount(); i++) {
                if (mButtonsGrid.getChildAt(i) instanceof DeckToggleButtonCustom) {
                    DeckToggleButtonCustom deckToggleButtonCustom = (DeckToggleButtonCustom) mButtonsGrid.getChildAt(i);
                    Object tag = deckToggleButtonCustom.getTag();

                    updateButtons(tag != null && tag.equals(
                            BUTTON_TAG_PREFIX + deckButton.getButtonId()) ? deckButton : null,
                            deckToggleButtonCustom);
                }
            }
        }
    }

    private void updateButtons(DeckButton selectedDeckButton, DeckToggleButtonCustom gridDeckButton) {
        if (selectedDeckButton != null) {
            gridDeckButton.setButtonInfo(selectedDeckButton);
            gridDeckButton.setOnClickListener(this::onButtonClick);
        }
    }

    private String normalizeColor(String color) {
        if (color.length() > 7)
            return color.substring(0, color.length() - 2);

        return color;
    }

    private void onButtonClick(View button) {
        if (button instanceof DeckToggleButtonCustom) {
            DeckToggleButtonCustom deckToggleButtonCustom = (DeckToggleButtonCustom) button;

            if (deckToggleButtonCustom.isVoicemeeter) {
                VoicemeeterVolumeDialog voicemeeterVolumeDialog = new VoicemeeterVolumeDialog(deckToggleButtonCustom);
                voicemeeterVolumeDialog.show(getChildFragmentManager(), "VoicemeeterVolumeDialog");
            } else {
                if (deckToggleButtonCustom.getButtonCommand() != null &&
                        !deckToggleButtonCustom.getButtonCommand().isEmpty())
                    ConnectionControl.getInstance().sendMessage(deckToggleButtonCustom.getButtonCommand());
            }
        }
    }
}