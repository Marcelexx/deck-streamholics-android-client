package com.streamholics.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.streamholics.common.Constants;
import com.streamholics.common.Regex;
import com.streamholics.control.ConnectionControl;
import com.streamholics.control.IConnectionControl;
import com.streamholics.control.SharedPreferencesControl;
import com.streamholics.event.CloseBoardActivityEvent;
import com.streamholics.event.ConnectionErrorEvent;
import com.streamholics.ui.LoadingOverlay;
import com.streamholics.util.KeyboardUtils;
import com.streamholics.util.QRCodeUtils;
import com.streamholics.util.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.streamholics.common.ExtraKeys.BOARDS_JSON;

public class MainActivity extends AppCompatActivity implements IConnectionControl {
    private EditText etServerIp;
    private FrameLayout flProgressOverlay;
    private Button btLastIPConnect;

    private String boardJsonString = "";

    private boolean isNewIp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etServerIp = findViewById(R.id.etServerIp);
        flProgressOverlay = findViewById(R.id.flProgressOverlay);
        btLastIPConnect = findViewById(R.id.btLastIPConnect);

        String lastConnectedIp = SharedPreferencesControl.getInstance().getLastIp(this);

        if (lastConnectedIp != null && !lastConnectedIp.equals("")) {
            String lastConnectedIpMessage = getString(R.string.connect_last_ip) + lastConnectedIp;
            btLastIPConnect.setText(lastConnectedIpMessage);
        } else
            btLastIPConnect.setEnabled(false);

        setupInputFields();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onClickConnect(View event) {
        KeyboardUtils.closeKeyboard(this, event);

        if (!etServerIp.getText().toString().isEmpty())
            startSocketConnection(etServerIp.getText().toString(), true);
    }

    public void onClickScan(View event) {
        KeyboardUtils.closeKeyboard(this, event);
        QRCodeUtils.startQRCodeScanner(this);
    }

    public void onClickConnectLastIp(View event) {
        KeyboardUtils.closeKeyboard(this, event);

        startSocketConnection(SharedPreferencesControl.getInstance().getLastIp(this), false);
    }

    private void setupInputFields() {
        etServerIp.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                Button btConnect = findViewById(R.id.btConnect);
                btConnect.setEnabled(validateIpAddress(text.toString()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        etServerIp.setOnFocusChangeListener((view, isFocused) -> {
            if (!isFocused)
                KeyboardUtils.closeKeyboard(this, view);
        });
    }

    private boolean validateIpAddress(String ipAddress) {
        return ipAddress.matches(Regex.IP_ADDRESS_REGEX);
    }

    private void startSocketConnection(String ipAddress, boolean isNewIp) {
        this.isNewIp = isNewIp;
        LoadingOverlay.startLoading(this, flProgressOverlay);
        ConnectionControl.getInstance().startSocket(ipAddress, this);
    }

    private void launchButtonsActivity() {
        LoadingOverlay.stopLoading(this, flProgressOverlay);

        if (isNewIp) {
            String lastConnectedIp = etServerIp.getText().toString();
            if (!lastConnectedIp.equals("")) {
                SharedPreferencesControl.getInstance().setLastIp(this, lastConnectedIp);
                String lastConnectedIpMessage = getString(R.string.connect_last_ip) + lastConnectedIp;

                runOnUiThread(() -> {
                    btLastIPConnect.setText(lastConnectedIpMessage);
                    btLastIPConnect.setEnabled(true);
                });
            }
        }

        EventBus.getDefault().post(new CloseBoardActivityEvent());
        Intent intent = new Intent(this, BoardsActivity.class);
        intent.putExtra(BOARDS_JSON, boardJsonString);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");

                if (contents != null && contents.matches(Regex.IP_ADDRESS_REGEX)) {
                    etServerIp.setText(contents);
                    startSocketConnection(contents, true);
                }
            }
        }
    }

    @Override
    public void readJsonLine(String line) {
        if (line.equals(Constants.JSON_BOARD_BEGINNING_WORD)) {
            boardJsonString = "";
            return;
        }

        if (line.equals(Constants.JSON_BOARD_ENDING_WORD))
            launchButtonsActivity();

        boardJsonString += line;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onConnectionErrorEvent(ConnectionErrorEvent event) {
        LoadingOverlay.stopLoading(this, flProgressOverlay);
        ToastUtils.makeToast(this, R.string.socket_connection_error);
    }
}