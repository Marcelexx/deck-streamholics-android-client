package com.streamholics.presentation.activity;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.streamholics.common.Constants;
import com.streamholics.control.ConnectionControl;
import com.streamholics.event.CloseBoardActivityEvent;
import com.streamholics.ui.ViewPagerCustom;
import com.streamholics.ui.adapter.ViewPagerCustomAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.streamholics.common.ExtraKeys.BOARDS_JSON;

public class BoardsActivity extends AppCompatActivity {
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boards);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        loadAdsScreen();
        loadViewPager();
    }

    @Override
    public void onBackPressed() {
        ConnectionControl.getInstance().stopSocket();
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void loadAdsScreen() {
        interstitialAd = new InterstitialAd(this);
//        interstitialAd.setAdUnitId(getString(R.string.interstitial_api_key));
        interstitialAd.setAdUnitId(getString(R.string.interstitial_api_test_key));
//        interstitialAd.loadAd(new AdRequest.Builder().build());
//        interstitialAd.loadAd(new AdRequest.Builder().addTestDevice(Constants.ADMOB_INTERSTITIAL_TEST_DEVICE).build());

        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                if (interstitialAd.isLoaded())
                    interstitialAd.show();
            }
        });
    }

    private void loadViewPager() {
        ViewPagerCustomAdapter adapter = new ViewPagerCustomAdapter(getSupportFragmentManager());
        adapter.setBoardList(getBoardsJson());
        ViewPagerCustom viewPagerCustom = findViewById(R.id.vp_main_layout);
        viewPagerCustom.setAdapter(adapter);
    }

    private String getBoardsJson() {
        return getIntent().getStringExtra(BOARDS_JSON);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCloseBoardActivityEvent(CloseBoardActivityEvent event) {
        finish();
    }
}