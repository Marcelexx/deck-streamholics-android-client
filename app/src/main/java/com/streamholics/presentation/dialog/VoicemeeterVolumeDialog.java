package com.streamholics.presentation.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.streamholics.control.ConnectionControl;
import com.streamholics.presentation.activity.R;
import com.streamholics.ui.DeckToggleButtonCustom;

import java.util.Objects;

public class VoicemeeterVolumeDialog extends DialogFragment {
    public VoicemeeterVolumeDialog(DeckToggleButtonCustom deckToggleButtonCustom) {
        Bundle args = new Bundle();
        args.putParcelable("deckButton", deckToggleButtonCustom);
        args.putString("channel", deckToggleButtonCustom.voicemeeterChannel);
        args.putDouble("value", deckToggleButtonCustom.voicemeeterValue);
        setArguments(args);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()), android.R.style.Theme_Translucent_NoTitleBar);
        @SuppressLint("InflateParams") final View view = getActivity().getLayoutInflater().inflate(R.layout.volume_bar, null);
        final Drawable d = new ColorDrawable(Color.WHITE);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(d);
            dialog.getWindow().setContentView(view);
        }

        final WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.gravity = Gravity.CENTER;

        dialog.setCanceledOnTouchOutside(true);

        SeekBar voicemeeterVolumeSeekBar = dialog.findViewById(R.id.sb_voicemeeter_volume);
        voicemeeterVolumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (getArguments() != null) {
                    double volumeInDbs = calculateDbFromSeekBar(progress);
                    String channel = getArguments().getString("channel");
                    ConnectionControl.getInstance().sendMessage("Voicemeeter " + channel + " + " + volumeInDbs);

                    DeckToggleButtonCustom deckButton = getArguments().getParcelable("deckButton");
                    if (deckButton != null)
                        deckButton.voicemeeterValue = volumeInDbs;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        if (getArguments() != null)
            voicemeeterVolumeSeekBar.setProgress(calculateSeekBarFromDb(getArguments().getDouble("value")));

        return dialog;
    }

    private double calculateDbFromSeekBar(int seekValue) {
        return ((63.0 * seekValue) - 6000.0) / 100.0;
    }

    private int calculateSeekBarFromDb(double voicemeeterValue) {
        return (int) (((100 * voicemeeterValue) + 6000) / 63);
    }
}